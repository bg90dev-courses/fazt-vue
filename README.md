# Curso Vue Fazt

![Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![](https://img.shields.io/badge/Status-finished-purple.svg)

Curso de Vue por FaztWeb

https://www.youtube.com/watch?v=mfvNpUYV04U&list=PLL0TiOXBeDajWIEXDUvQbzjV4D4GiruLy

## Built With
<a href="https://vuejs.org/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vueJS.png" width=50 alt="VueJS"></a>

### Platforms
<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Author
* **Borja Gete** - <borjag90dev@gmail.com>